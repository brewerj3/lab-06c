###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 06c - Countdown - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author @todo Joshua Brewer <brewerj3@hawaii.edu>
# @date   @todo 20_Feb_2022
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = countdown


all:  $(TARGET)


CC     = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)

countdownMath.o: countdownMath.c countdownMath.h
	$(CC) $(CFLAGS) -c countdownMath.c

main.o: main.c countdown.h countdownMath.h
	$(CC) $(CFLAGS) -c main.c


countdown: main.o countdownMath.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o countdownMath.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o
